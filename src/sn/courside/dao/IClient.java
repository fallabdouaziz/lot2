package sn.courside.dao;

import sn.courside.entities.Client;

import java.util.List;

public interface IClient {
     int add(Client client);
     List<Client> listClient();
     int update(Client client);
     Client findById(String id);
}
