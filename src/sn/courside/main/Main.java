package sn.courside.main;

import sn.courside.dao.ClientImpl;
import sn.courside.dao.IClient;
import sn.courside.entities.Client;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        IClient clientdao = new ClientImpl();
        Client client= new Client();
        int okmessage=0;
        int messageExiste=0;
        List<Client> list= new ArrayList<>();
        list= clientdao.listClient();
        String[] tabfile= null;
        String[] tabclient= null;
        Path path= Paths.get("src/sn/courside/ressources/client");
        if(Files.exists(path)) {
            if(path.toRealPath(LinkOption.NOFOLLOW_LINKS).toString().endsWith("client")){
                Scanner scanner= new Scanner(path);
                if (!scanner.hasNext()){
                    System.out.println("Le fichier est vide");
                }
                else{
                    while (scanner.hasNext()){
                        tabfile= scanner.nextLine().split(";");
                        if (tabfile.length % 4==0){
                            for (int i=0;i<tabfile.length;i++){
                                //System.out.println(tabfile[0]+" "+i);
                               if (clientdao.findById(tabfile[0]).getId()==null){
                                   int ok=clientdao.add(new Client(tabfile[0],tabfile[1],tabfile[2],tabfile[3]));
                                   if (ok==1){
                                       okmessage=1;
                                       break;
                                   }
                                   break;
                               }
                               else {
                                   messageExiste=1;
                                   break;
                               }
                            }
                        }
                        else {
                            System.out.println("Veuillez Remplir tous les champs");
                        }

                    }
                    if (messageExiste==1){
                        System.out.println("Existent deja dans la base");
                    }
                    if (okmessage==1){
                        System.out.println("Client ajouté dans la base de donnée");
                    }
                }

            }
            else{
                System.out.println("Seul le fichier client.txt est accepté");
            }

        }
        else {
            System.out.println("Le fichier n'existe pas");
        }
        /*String[] tabclient=null;
        while (scanner.hasNext()){
            tabclient= scanner.nextLine().split(";");
        }
        for (Client cl:list) {
            System.out.println(cl.getId());
            if (cl.getId()==tabclient[0]){
                System.out.println("existe deja");
            }
            else{
                System.out.println("nouveau élément");
                client.setId(tabclient[0]);
                client.setNom(tabclient[1]);
                client.setPrenom(tabclient[2]);
                client.setAdresse(tabclient[3]);
                int ok=clientdao.add(client);
                if (ok!=0){
                    System.out.println("client ajouté á la base de données");
                }
                else{
                    System.out.println("erreur lord de l'ajout");
                }
            }
        }*/

    }
}
